# frozen_string_literal: true

class TwitterController < ApplicationController
  def search
    term = params[:search]
    return unless term.present?

    @tweets = TweetsRetriever.tweets(term)
    @paginatable_tweets = Kaminari.paginate_array(@tweets).page(params[:page])
  end
end
