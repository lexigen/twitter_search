# frozen_string_literal: true

class TweetsRetriever
  def self.tweets(term)
    TwitterApi.new.search(term)
  end
end
