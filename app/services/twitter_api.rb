# frozen_string_literal: true

class TwitterApi
  NUMBER_OF_RESULTS = 100
  RESULT_TYPE = 'recent'

  def client
    @client ||= Twitter::REST::Client.new do |config|
      config.consumer_key        = ENV['API_KEY']
      config.consumer_secret     = ENV['API_SECRET']
    end
  end

  def search(term)
    client.search(term, result_type: RESULT_TYPE)
          .take(NUMBER_OF_RESULTS)
          .map(&:attrs)
  end
end
