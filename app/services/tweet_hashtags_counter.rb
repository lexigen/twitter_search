# frozen_string_literal: true

class TweetHashtagsCounter
  attr_reader :tweets

  TOP_COUNT = 10

  def initialize(tweets)
    @tweets = tweets
  end

  def top_hashtags
    hashtags = organize_hashtags
    hashtags.sort_by(&:last).reverse.first(TOP_COUNT).to_h
  end

  private

  def organize_hashtags
    tweets.each_with_object(Hash.new(0)) do |tweet, hashtags|
      tweet[:entities][:hashtags].each { |ht| hashtags[ht[:text]] += 1 }
    end
  end
end
