# frozen_string_literal: true

module TwitterHelper
  def top_hashtags(tweets)
    TweetHashtagsCounter.new(tweets).top_hashtags
  end
end
