# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TwitterHelper do
  let(:tweets) do
    [
      {
        text: '#hashtag_1 some text #hashtag_2 and #hashtag_3',
        entities:
          {
            hashtags:
              [
                { text: 'hashtag_1' },
                { text: 'hashtag_2' },
                { text: 'hashtag_3' }
              ]
          },
        user:
          {
            name: 'John',
            screen_name: 'john_doe'
          }
      },
      {
        text: '#hashtag_1 some text #hashtag_4 and #hashtag_5',
        entities:
          {
            hashtags:
              [
                { text: 'hashtag_1' },
                { text: 'hashtag_4' },
                { text: 'hashtag_5' }
              ]
          },
        user:
          {
            name: 'Bill',
            screen_name: 'billy_joel'
          }
      },
      {
        text: '#hashtag_1 some text #hashtag_5 and #hashtag_6',
        entities:
          {
            hashtags:
              [
                { text: 'hashtag_1' },
                { text: 'hashtag_5' }
              ]
          },
        user:
          {
            name: 'Josh',
            screen_name: 'josh_walker'
          }
      }
    ]
  end

  let(:sorted_hashtags) do
    { 'hashtag_1' => 3, 'hashtag_5' => 2, 'hashtag_2' => 1,
      'hashtag_3' => 1, 'hashtag_4' => 1 }
  end

  describe '# organize_hashtags' do
    it 'returns hash with number of each hashtag' do
      expect(helper.top_hashtags(tweets)).to eq(sorted_hashtags)
    end
  end
end
