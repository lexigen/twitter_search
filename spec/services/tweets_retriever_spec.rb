# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TweetsRetriever do
  let(:twitter_client) { instance_double(TwitterApi) }
  let(:twitter_response) do
    [{ text: 'I shoot RAW',
       user: {
         screen_name: 'froknowsphoto'
       } }]
  end

  before do
    allow(twitter_client)
      .to receive(:search)
      .with('raw').and_return(twitter_response)
  end

  it 'returns tweets via API request' do
    expect(described_class.tweets('raw')).to eq(twitter_response)
  end
end
