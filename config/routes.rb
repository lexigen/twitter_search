Rails.application.routes.draw do
  get 'twitter/search'
  root to: 'twitter#search'
end
